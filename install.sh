#!/bin/bash

pip3 install python-vlc

wget https://gitlab.com/cowdenet/cowpi/jingle-boot/-/archive/main/jingle-boot-main.zip
unzip jingle-boot-main.zip -d ~/.config/
rm jingle-boot-main.zip
mv ~/.config/jingle-boot-main/ ~/.config/jingle-boot/


mkdir --parents ~/.config/systemd/user/
mv ~/.config/jingle-boot/jingleboot.service ~/.config/systemd/user/
systemctl --user enable jingleboot.service

mkdir --parents ~/Music/
mv ~/.config/jingle-boot/jingles ~/Music/

# Jingle Boot

This can be used by those who have Raspberry Pi and want to add a little personal touch to their system by customising the boot up sound and play some funny jingles.

## Installation Instructions

*Quick Jist* - Download install.sh file and execute it in terminal

1. Open terminal (by pressing Ctrl+Alt+T)
2. Run the following command
```bash
wget https://gitlab.com/cowdenet/cowpi/jingle-boot/-/raw/main/install.sh?inline=false -v -O install.sh && bash install.sh && rm install.sh

```
3. Reboot to hear a random jingle

import os
import vlc
import glob
import random
from time import sleep

homedir = os.path.expanduser("~")
jingles = homedir+"/Music/jingles/*"
formats = [".mp3",".ogg"]
playlist= []

for type in formats:
        playlist.extend(glob.glob(jingles+type))
p = vlc.MediaPlayer(random.choice(playlist))

p.play()
sleep(1)
duration=p.get_length()/1000
sleep(duration)
